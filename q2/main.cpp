#include 	"stdio.h"
#include 	"x86intrin.h"
#include    "math.h"
#include    <sys/time.h>

#define VECTOR_SIZE 1048576	

int main (void)
{
    printf("Student1 No. : 810196684, Student2 No. : 810196683\n");
	struct timeval startS, endS, startV, endV;

	float fSTmpRes[4];
	float fSResMean;
	float fSResStd;

	float fVResMean;
	float fVResStd;

	float *fArray;
	fArray = new float [VECTOR_SIZE];

	if (!fArray) {
		printf ("Memory allocation error!!\n");
		return 1;
	}
	// Initialize vectors with random numbers
	for (long i = 0; i < VECTOR_SIZE; i++)
		fArray[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/100.0));

	// Serial implementation
	gettimeofday(&startS, NULL);

	fSTmpRes[0] = fSTmpRes[1] = fSTmpRes[2] = fSTmpRes[3] = 0.0;
	for (long i = 0; i < VECTOR_SIZE; i+=4)
		fSTmpRes[0] += fArray[i];
	for (long i = 0; i < VECTOR_SIZE; i+=4)
		fSTmpRes[1] += fArray[i + 1];
	for (long i = 0; i < VECTOR_SIZE; i+=4)
		fSTmpRes[2] += fArray[i + 2];
	for (long i = 0; i < VECTOR_SIZE; i+=4)
		fSTmpRes[3] += fArray[i + 3];
	fSResMean = fSTmpRes[0] / (float)VECTOR_SIZE + fSTmpRes[1] / (float)VECTOR_SIZE;
    fSResMean += fSTmpRes[2] / (float)VECTOR_SIZE + fSTmpRes[3] / (float)VECTOR_SIZE;

    // standard deviation
    fSTmpRes[0] = fSTmpRes[1] = fSTmpRes[2] = fSTmpRes[3] = 0.0;
	for (long i = 0; i < VECTOR_SIZE; i+=4)
		fSTmpRes[0] += (fArray[i] - fSResMean)*(fArray[i] - fSResMean);
	for (long i = 0; i < VECTOR_SIZE; i+=4)
		fSTmpRes[1] += (fArray[i + 1] - fSResMean)*(fArray[i + 1] - fSResMean);
	for (long i = 0; i < VECTOR_SIZE; i+=4)
		fSTmpRes[2] += (fArray[i + 2] - fSResMean)*(fArray[i + 2] - fSResMean);
	for (long i = 0; i < VECTOR_SIZE; i+=4)
		fSTmpRes[3] += (fArray[i + 3] - fSResMean)*(fArray[i + 3] - fSResMean);

	fSResStd = fSTmpRes[0] / (float)VECTOR_SIZE + fSTmpRes[1] / (float)VECTOR_SIZE + fSTmpRes[2] / (float)VECTOR_SIZE + fSTmpRes[3] / (float)VECTOR_SIZE;
    
    fSResStd = std::sqrt(fSResStd);
	gettimeofday(&endS, NULL);
    
	// Parallel implementation
	gettimeofday(&startV, NULL);

	__m128 vec;
	__m128 avg = _mm_set1_ps(0.0f);
    __m128 number = _mm_set1_ps(1048576.0f);
	for (long i = 0; i < VECTOR_SIZE; i+=4) {
		vec = _mm_loadu_ps (&fArray[i]);
		avg = _mm_add_ps (avg, vec);
	}
    avg = _mm_div_ps(avg, number);
	avg = _mm_hadd_ps (avg, avg);
	avg = _mm_hadd_ps (avg, avg);
	fVResMean = _mm_cvtss_f32 (avg);

    //standard deviation
	__m128 std = _mm_set1_ps(0.0f);
	for (long i = 0; i < VECTOR_SIZE; i+=4) {
		vec = _mm_loadu_ps (&fArray[i]);
        vec = _mm_sub_ps(vec, avg);
        vec = _mm_mul_ps(vec, vec);
        std = _mm_add_ps(std, vec);
	}
    std = _mm_div_ps(std, number);
	std = _mm_hadd_ps (std, std);
	std = _mm_hadd_ps (std, std);
    std = _mm_sqrt_ps (std);
	fVResStd = _mm_cvtss_f32 (std);
	gettimeofday(&endV, NULL);


	printf ("\nSerial Result: mean= %f, Std= %f \nParallel Result: mean= %f, Std= %f\n", fSResMean, fSResStd, fVResMean, fVResStd);
    long secondsS = (endS.tv_sec - startS.tv_sec);
	long microsS = ((secondsS * 1000000) + endS.tv_usec) - (startS.tv_usec);
    
    long secondsV = (endV.tv_sec - startV.tv_sec);
	long microsV = ((secondsV * 1000000) + endV.tv_usec) - (startV.tv_usec);

	printf ("Serial Run time = %ld sec and %ld mSec \n", secondsS, microsS);
	printf ("Parallel Run time = %ld sec and %ld mSec \n", secondsV, microsV);
	printf ("Speedup = %f\n\n", (float) microsS/(float) microsV);

	return 0;
}
