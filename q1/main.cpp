#include 	"stdio.h"
#include 	"x86intrin.h"
#include    "time.h"
#include    <sys/time.h>

#define VECTOR_SIZE 1048576

int main (void)
{
    printf("Student1 No. : 810196684, Student2 No. : 810196683\n");

	struct timeval startS, endS, startV, endV;

	float fSTmpRes[4];
	int fSTmpResIndex[4];
	float fSRes;
    int fSResIndex;

	float *fArray;
	fArray = new float [VECTOR_SIZE];

	if (!fArray) {
		printf ("Memory allocation error!!\n");
		return 1;
	}
	// Initialize vectors with random numbers
    srand(time(0));
	for (long i = 0; i < VECTOR_SIZE; i++)
		fArray[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/100.0));

    // Serial implementation
	gettimeofday(&startS, NULL);

    fSTmpRes[0] = fArray[0];
    fSTmpRes[1] = fArray[1];
    fSTmpRes[2] = fArray[2];
    fSTmpRes[3] = fArray[3];

    fSTmpResIndex[0] = 0;
    fSTmpResIndex[1] = 1;
    fSTmpResIndex[2] = 2;
    fSTmpResIndex[3] = 3;

	for (long i = 0; i < VECTOR_SIZE; i+=4) {
		fSTmpResIndex[0] = fSTmpRes[0] > fArray[i] ? fSTmpResIndex[0] : i;
		fSTmpRes[0] = fSTmpRes[0] > fArray[i] ? fSTmpRes[0] : fArray[i];
    }
	for (long i = 0; i < VECTOR_SIZE; i+=4) {
		fSTmpResIndex[1] = fSTmpRes[1] > fArray[i + 1] ? fSTmpResIndex[1] : i + 1;
		fSTmpRes[1] = fSTmpRes[1] > fArray[i + 1] ? fSTmpRes[1] : fArray[i + 1];
    }
	for (long i = 0; i < VECTOR_SIZE; i+=4) {
		fSTmpResIndex[2] = fSTmpRes[2] > fArray[i + 2] ? fSTmpResIndex[2] : i + 2;
		fSTmpRes[2] = fSTmpRes[2] > fArray[i + 2] ? fSTmpRes[2] : fArray[i + 2];
    }
	for (long i = 0; i < VECTOR_SIZE; i+=4) {
		fSTmpResIndex[3] = fSTmpRes[3] > fArray[i + 3] ? fSTmpResIndex[3] : i + 3;
		fSTmpRes[3] = fSTmpRes[3] > fArray[i + 3] ? fSTmpRes[3] : fArray[i + 3];
    }

    fSRes = fSTmpRes[0];
    fSResIndex = fSTmpResIndex[0];
    for (long i = 1; i < 4; i++) {
        if (fSRes < fSTmpRes[i]) {
            fSRes = fSTmpRes[i];
            fSResIndex =fSTmpResIndex[i];
        }
    }

	gettimeofday(&endS, NULL);



    // Parallel implementation

    __m128i indices, increament, fVTmpIndex;
	gettimeofday(&startV, NULL);
	
	indices = _mm_setr_epi32 (0, 1, 2, 3);
    fVTmpIndex = indices;
	increament  = _mm_set1_epi32 (4.0);

	__m128 vec;
	__m128 maxCmp = _mm_loadu_ps (&fArray[0]);
    __m128 maskF;

	for (long i = 4; i < VECTOR_SIZE; i+=4) {
        indices = _mm_add_epi32(indices, increament);
		vec = _mm_loadu_ps (&fArray[i]);
        maskF = _mm_cmpgt_ps(vec, maxCmp);

        const __m128i maskI = _mm_castps_si128(maskF);
        fVTmpIndex = _mm_blendv_epi8(fVTmpIndex, indices, maskI);

        maxCmp = _mm_max_ps(maxCmp, vec);
        
	}

    float fVRes;
    float fVTmpRes[4];
    int   fVResIndex;
    int   fVTmpResIndex[4];

    _mm_store_ps(fVTmpRes, maxCmp);
    _mm_store_si128((__m128i*)fVTmpResIndex, fVTmpIndex);
    

    fVRes = fVTmpRes[0];
    fVResIndex = fVTmpResIndex[0];

    for (long i = 1; i < 4; i++) {
        if (fVRes < fVTmpRes[i]) {
            fVRes = fVTmpRes[i];
            fVResIndex = fVTmpResIndex[i];
        }

    }
	gettimeofday(&endV, NULL);


	printf ("\nSerial Result array[%d]  = %f  \nParallel Result array[%d]  = %f\n", fSResIndex, fSRes, fVResIndex, fVRes);
   
    long secondsS = (endS.tv_sec - startS.tv_sec);
	long microsS = ((secondsS * 1000000) + endS.tv_usec) - (startS.tv_usec);
    
    long secondsV = (endV.tv_sec - startV.tv_sec);
	long microsV = ((secondsV * 1000000) + endV.tv_usec) - (startV.tv_usec);

	printf ("Serial Run time = %ld sec and %ld mSec \n", secondsS, microsS);
	printf ("Parallel Run time = %ld sec and %ld mSec \n", secondsV, microsV);
	printf ("Speedup = %f\n\n", (float) microsS/(float) microsV);
    
    
    
    return 0;
}